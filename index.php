<?php
    session_start();
    if( isset($_SESSION['token'])==false)
    {
        $_SESSION['token'] = bin2hex(random_bytes(32)); //Token CSRF
    }
    if(isset($_SESSION['role']))
    {
        $role=$_SESSION['role'];
    }
try {

    require('controller/controller.php');
   
    if(isset($_GET['path']))
    {
        $path=test_input($_GET['path']);
        switch($path)
        {
            case "admin":
                require("./controller/adminController.php");
            break;
            case "gestionnaire":
                require("./controller/gestionnaireController.php");
                gestionnaireController();
            break;
        }
    }
    else{
        mainController();
    }

} catch (Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>