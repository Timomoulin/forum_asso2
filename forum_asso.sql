-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 19 avr. 2021 à 11:26
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `forum_asso2`
--

-- --------------------------------------------------------

--
-- Structure de la table `activités`
--

DROP TABLE IF EXISTS `activités`;
CREATE TABLE IF NOT EXISTS `activités` (
  `NumActivité` int(11) NOT NULL AUTO_INCREMENT,
  `NomActivité` varchar(50) NOT NULL,
  `NumTypeActivité` int(11) NOT NULL,
  PRIMARY KEY (`NumActivité`),
  KEY `NumTypeActivité` (`NumTypeActivité`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `activités`
--

INSERT INTO `activités` (`NumActivité`, `NomActivité`, `NumTypeActivité`) VALUES
(1, 'Accompagnement à la scolarité', 1),
(2, 'Arts graphiques et plastiques', 2),
(3, 'Atelier d’écriture', 3),
(4, 'Ateliers enfants', 4),
(5, 'Ateliers familles', 5),
(6, 'Ateliers jeunes', 6),
(7, 'Bibliotheques', 7),
(8, 'Chant et/ou Chorale', 8),
(9, 'Chorale', 9),
(10, 'Danse classique', 10),
(11, 'Rock’ n’ roll', 10),
(12, 'Salsa cubaine', 10),
(13, 'Environnement', 11),
(14, 'Informatique – Multimédia', 12),
(15, 'Loisirs rencontres, visites et excursions', 13),
(16, 'Loisirs séniors', 14),
(17, 'Ludothèques', 15),
(18, 'Musique Instruments', 16),
(19, 'Musique Eveil musical et corporel', 16),
(20, 'Musique, éveil, ateliers', 16),
(21, 'Musique: création musicale', 16),
(22, 'Philosophie', 17),
(23, 'Photographie', 18),
(24, 'Théâtre: Jeu théâtral', 19),
(25, 'Théâtre: Art dramatique', 19),
(26, 'Théâtre', 19),
(27, 'Comedie musicale', 20);

-- --------------------------------------------------------

--
-- Structure de la table `associations`
--

DROP TABLE IF EXISTS `associations`;
CREATE TABLE IF NOT EXISTS `associations` (
  `NumAssociation` int(11) NOT NULL AUTO_INCREMENT,
  `NomAssociation` varchar(50) NOT NULL,
  `AssociationPrivée` tinyint(1) NOT NULL DEFAULT '1',
  `AdresseElectroniqueAssociation` varchar(50) DEFAULT NULL,
  `SiteAssociation` varchar(50) DEFAULT NULL,
  `Description` text,
  PRIMARY KEY (`NumAssociation`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `associations`
--

INSERT INTO `associations` (`NumAssociation`, `NomAssociation`, `AssociationPrivée`, `AdresseElectroniqueAssociation`, `SiteAssociation`, `Description`) VALUES
(1, 'OMC-MAISON POUR TOUS D ALFORT', 0, 'maisonpourtousalfort@gmail.com', '', 'Aide aux devoirs, activités culturelles, ludiques et éducatives, découverte des différentes techniques des arts graphiques et plastiques, initiations de base informatique et internet.'),
(2, 'ATELIER DE DESSIN ET PEINTURE POUR TOUS', 1, 'adept94700ma@gmail.com', 'www.adept94.fr', 'Etude du dessin, du fusain, du pastel, de la peinture a l huile ou acrylique, de la sanguine etc… De 18 à 90 ans.'),
(3, 'MA QUETE (LA RELEVE BARIOLEE)', 1, 'nebildaghsen@gmail.com', 'www.larelevebariolee.fr', 'Poésie, slam, écriture scénario Ouvert à tous les amoureux de l écriture, qu elle soit poétique ou fictionnelle.'),
(4, 'OMC-CSC LA CROIX DES OUCHES', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Ateliers de création en famille. Un moment de créativité, d échanges et de partage autour d activités manuelles. En famille à partir de 3 ans.'),
(5, 'OMC-CSC LES PLANETES', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Permettre aux jeunes ados d accéder à la culture et aux loisirs par le biais d activités sur le centre ou des sorties à thème.'),
(6, 'MÉDIATHÈQUE ANDRÉ MALRAUX', 0, '', 'https://bibliotheques.maisons-alfort.fr', 'Un réseau de lecture publique à Maisons Alfort : prêt de livres, magazines, de CD et de DVD ; des expositions, des rencontres; des spectacles et lectures pour les enfants, un espace numérique avec accès à internet ; des espaces de travail.'),
(7, 'CONSERVATOIRE HENRI DUTILLEUX', 0, 'conservatoire.ville@maisons-alfort.fr ', 'www.maisons-alfort.fr', 'Ateliers de chant lyrique, art de la scène, chorale d enfants, instruments, éveil musical et corporel, jeu théâtral et art dramatique.'),
(8, 'ECOLE DE MUSIQUE MODERNE ET ANCIENNE', 1, 'ecoledemusique.emma@gmail.com', 'www.ecoledemusique-emma.com', 'Ateliers de musiques actuelles, chant lyrique, chorales enfants, ados et adultes, théâtre et improvisation.'),
(9, 'ARS MUSICA MANSIONUM', 1, '', 'arsmusicamansionum.org', 'Chant chorale répertoire musique classique de toutes époques, pas de niveau musical exigé.'),
(10, 'CLUB DE DANSE SPORTIVE DE MAISONS-ALFORT', 1, 'cdsma94@gmail.com', 'www.cdsma.com', 'Ateliers de danse sportive à partir de 16 ans.'),
(11, 'ASSOCIATION PROTECTION DE L ENVIRONNEMENT', 1, 'apema@neuf.fr', '', 'Association de défense contre les nuisances : bruits, qualité de l air, visuelle …'),
(12, 'UNIVERSITE INTER-AGES', 1, '', 'www.uia94.fr', 'Apprendre, découvrir l utilisation des outils numériques (ordi PC, MAC, téléphones, tablettes); Etude de la philosophie occidentale de Socrate, Platon, Aristote, Nietzsche, Heidegger et Foucault.'),
(13, 'ASSOCIATION RENCONTRES LOISIRS', 1, '', '', 'Une activité par mois le dimanche : excursion à la journée, deux repas dans l année et un loto goûter.'),
(14, 'OMC INFO-Séniors', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Loisirs rencontres : visites culturelles, après-midi dansants, gymnastique séniors, chorales, multimédia, voyages.'),
(15, 'LUDOTHEQUE DE LA MAISON DE L ENFANT', 0, 'ludothequemaisondelenfant@maisons-alfort.fr', '', 'Espace de jeux réservé aux enfants de 0 à 5 ans.'),
(16, 'OMC-CSC LIBERTE', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Atelier d écriture, composition musicale avec la création d une bande son et d instruments, enregistrement. Participation aux fêtes de quartier.'),
(17, 'PHOTO-CLUB MAISONNAIS', 1, 'photoclub.maisonnais@orange.fr', '', 'Faire découvrir et échanger sur la photographie argentique et numérique, noir&blanc et couleur. Participer à des salons, des concours.'),
(18, 'AN-K ATELIER DE COMEDIE MUSICALE DE MAISONS-ALFORT', 1, 'assoank@gmail.com', 'an-k.fr', 'Atelier de comédie musicale : travail autour d une comédie musicale humoristique inédite, cours de comédie, chant et danse, en vue d une représentation en fin d année.');

-- --------------------------------------------------------

--
-- Structure de la table `avoirrole`
--

DROP TABLE IF EXISTS `avoirrole`;
CREATE TABLE IF NOT EXISTS `avoirrole` (
  `NumAssociation` int(11) NOT NULL,
  `NumContact` int(11) NOT NULL,
  `NumStatut` int(11) NOT NULL,
  PRIMARY KEY (`NumAssociation`,`NumContact`,`NumStatut`),
  KEY `NumContact` (`NumContact`),
  KEY `NumStatut` (`NumStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `avoirrole`
--

INSERT INTO `avoirrole` (`NumAssociation`, `NumContact`, `NumStatut`) VALUES
(1, 1, 3),
(2, 2, 2),
(2, 3, 6),
(2, 4, 9),
(3, 5, 1),
(3, 6, 10),
(4, 7, 4),
(5, 8, 3),
(6, 9, 3),
(7, 10, 4),
(8, 11, 2),
(8, 12, 4),
(9, 13, 1),
(9, 14, 9),
(10, 15, 1),
(11, 16, 1),
(11, 17, 9),
(11, 18, 7),
(12, 19, 2),
(12, 20, 9),
(13, 21, 2),
(13, 22, 6),
(14, 23, 10),
(15, 24, 10),
(16, 25, 3),
(17, 26, 2),
(18, 27, 2);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `NumContact` int(11) NOT NULL AUTO_INCREMENT,
  `CivilitéContact` varchar(50) NOT NULL,
  `NomContact` varchar(50) NOT NULL,
  `TelephoneContact` varchar(50) DEFAULT NULL,
  `AdresseElectroniqueContact` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NumContact`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`NumContact`, `CivilitéContact`, `NomContact`, `TelephoneContact`, `AdresseElectroniqueContact`) VALUES
(1, 'MONSIEUR', 'SCHAEFFER', '01 45 18 39 90', 'maisonpourtousalfort@gmail.com'),
(2, 'MADAME', 'DEBOUX', '01 43 78 16 48', 'maite.deboux@gmail.com'),
(3, 'MADAME', 'ROTH', '01 43 96 36 84', 'bernadetteroth@orange.fr'),
(4, 'MADAME', 'WICIAK', '01 48 98 37 87', 'therese.wiciak2@orange.fr'),
(5, 'MONSIEUR', 'DAGHSEN', '06 95 60 41 88', ''),
(6, 'MADAME', 'MECILI', '06 60 61 92 08', 'leamecili@gmail.com'),
(7, 'MADAME', 'DEFOSSE ', '01 41 79 16 15', 'omc.csccroixdesouches@gmail.com'),
(8, 'MONSIEUR', 'HADROUGA', '01 42 07 38 68', 'omc.planetes2@wanadoo.fr'),
(9, 'MONSIEUR', 'EXEMPLE', '01 43 76 30 77', ''),
(10, 'MADAME', 'EXEMPLE', '01 53 48 10 17', 'conservatoire.ville@maisons-alfort.fr'),
(11, 'MADAME', 'BEAUCOUR', '', ''),
(12, 'MADAME', 'FAGOTTO', '01 77 99 37 11', ''),
(13, 'MONSIEUR', 'MANGIN', '01 43 75 15 26', 'cmangin.arsmusica@orange.fr'),
(14, 'MONSIEUR', 'OLINET', '01 43 75 52 95', 'emmanuel.olinet@orange.fr'),
(15, 'MONSIEUR', 'BOY', '06 37 60 31 26', ''),
(16, 'MONSIEUR', 'MOTTEAU', '01 49 77 66 36', 's.motteau@neuf.fr'),
(17, 'MONSIEUR', 'RASETTI', '06 70 55 72 74', 'luigi.rasetti@gmail.com'),
(18, 'MONSIEUR', 'RACLOT', '06 01 82 73 23', 'craclot@sfr.fr'),
(19, 'MADAME', 'PASCHE', '01 43 78 06 97', 'jeanninepasche@wanadoo.fr'),
(20, 'MADAME', 'MAILFAIT', '06 86 76 56 57', 'christinemail94@gmail.com'),
(21, 'MADAME', 'COVILLE', '06 34 57 52 46', 'solange1952@yahoo.fr'),
(22, 'MADAME', 'JOURNOU', '06 62 54 73 00', ''),
(23, 'MADAME', 'BENICHOU', '01 45 18 14 42', 'omc.infoseniors@gmail.com'),
(24, 'MADAME', 'SIMONINI', '01 41 79 07 05', ''),
(25, 'MONSIEUR', 'KIFOUCHE', '01 41 79 08 30', 'cscliberte@orange.fr'),
(26, 'MADAME', 'AUBERT', '', 'photoclub.maisonnais@orange.fr'),
(27, 'MADAME', 'LORRIC', '06 26 18 13 54', 'assoank@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `joursemaine`
--

DROP TABLE IF EXISTS `joursemaine`;
CREATE TABLE IF NOT EXISTS `joursemaine` (
  `JoursSemaine` varchar(50) NOT NULL,
  PRIMARY KEY (`JoursSemaine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `joursemaine`
--

INSERT INTO `joursemaine` (`JoursSemaine`) VALUES
('Dimanche'),
('Jeudi'),
('Lundi'),
('Mardi'),
('Mercredi'),
('Samedi'),
('Se renseigner auprès de l association'),
('Se renseigner auprès du conservatoire'),
('Vendredi');

-- --------------------------------------------------------

--
-- Structure de la table `lieux`
--

DROP TABLE IF EXISTS `lieux`;
CREATE TABLE IF NOT EXISTS `lieux` (
  `NumLieu` int(11) NOT NULL AUTO_INCREMENT,
  `NomLieu` varchar(50) NOT NULL,
  `AdresseLieu` varchar(50) NOT NULL,
  PRIMARY KEY (`NumLieu`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `lieux`
--

INSERT INTO `lieux` (`NumLieu`, `NomLieu`, `AdresseLieu`) VALUES
(1, 'MAISON POUR TOUS D ALFORT', '1 rue du Maréchal Juin'),
(2, 'MAISON POUR TOUS POMPIDOU', '12 rue Georges Gaumé'),
(3, 'CSC LA CROIX DES OUCHES', '33 avenue de la République'),
(4, 'CSC LES PLANETES', '149 rue Marc Sangnier'),
(5, 'CSC LES PLANETES', '56 rue de Mayenne 94000 Créteil'),
(6, 'MÉDIATHÈQUE ANDRÉ MALRAUX', '4 rue Albert Camus'),
(7, 'CONSERVATOIRE HENRI DUTILLEUX', '83 - 85 Rue Victor Hugo'),
(8, 'CENTRE CULTUREL CHARENTONNEAU', '107 avenue Gambetta'),
(9, 'Se renseigner auprès de l association', 'Se renseigner auprès de l association'),
(10, 'OMC - INFO-Séniors', '10 rue Bourgelat'),
(11, 'MAISON DE L ENFANT', '86 avenue Busteau'),
(12, 'MAISON DU COMBATTANT', '27 rue Jouët'),
(13, 'AU REZ-DE-CHAUSSEE', '4 rue du 18 Juin 1940'),
(14, 'ESPACE MULTIMEDIA', '11 bis Square Dufourmantelle');

-- --------------------------------------------------------

--
-- Structure de la table `pratique`
--

DROP TABLE IF EXISTS `pratique`;
CREATE TABLE IF NOT EXISTS `pratique` (
  `NumPratique` int(11) NOT NULL AUTO_INCREMENT,
  `NumActivité` int(11) NOT NULL,
  `NumAssociation` int(11) NOT NULL,
  `NumLieu` int(11) NOT NULL,
  `JoursSemaine` varchar(50) NOT NULL,
  `HeureDébut` time DEFAULT NULL,
  `HeureFin` time DEFAULT NULL,
  `TypePublic` varchar(50) DEFAULT NULL,
  `Details` text,
  PRIMARY KEY (`NumPratique`),
  KEY `JoursSemaine` (`JoursSemaine`),
  KEY `NumAssociation` (`NumAssociation`),
  KEY `NumActivité` (`NumActivité`),
  KEY `NumLieu` (`NumLieu`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pratique`
--

INSERT INTO `pratique` (`NumPratique`, `NumActivité`, `NumAssociation`, `NumLieu`, `JoursSemaine`, `HeureDébut`, `HeureFin`, `TypePublic`, `Details`) VALUES
(1, 1, 1, 1, 'Lundi', '16:15:00', '18:30:00', 'Enfants', 'Hors vacances scolaires'),
(2, 1, 1, 1, 'Mardi', '16:15:00', '18:30:00', 'Enfants', 'Hors vacances scolaires'),
(3, 1, 1, 1, 'Jeudi', '16:15:00', '18:30:00', 'Enfants', 'Hors vacances scolaires'),
(4, 2, 2, 1, 'Vendredi', '14:00:00', '17:30:00', 'Jeunes, Adultes, Seniors', ''),
(5, 2, 1, 1, 'Samedi', '14:30:00', '16:00:00', 'Enfants', ''),
(6, 2, 1, 1, 'Samedi', '16:00:00', '17:30:00', 'Enfants', ''),
(7, 2, 1, 1, 'Samedi', '17:45:00', '19:15:00', 'Enfants', ''),
(8, 4, 1, 1, 'Mercredi', '14:00:00', '16:00:00', 'Enfants', 'Davantage d activités pendant les vacances scolaires'),
(9, 11, 10, 1, 'Mercredi', '19:30:00', '20:30:00', 'Jeunes, Adultes', ''),
(10, 11, 10, 1, 'Mercredi', '20:30:00', '21:30:00', 'Jeunes, Adultes', ''),
(11, 11, 10, 1, 'Mercredi', '21:30:00', '22:30:00', 'Jeunes, Adultes', ''),
(12, 12, 10, 1, 'Jeudi', '19:00:00', '20:00:00', 'Jeunes, Adultes', ''),
(13, 12, 10, 1, 'Jeudi', '20:00:00', '21:00:00', 'Jeunes, Adultes', ''),
(14, 12, 10, 1, 'Jeudi', '21:00:00', '22:00:00', 'Jeunes, Adultes', ''),
(15, 14, 1, 1, 'Se renseigner auprès de l association', '09:00:00', '20:00:00', 'Adultes', ''),
(16, 2, 2, 2, 'Lundi', '19:00:00', '22:00:00', 'Jeunes, Adultes, Seniors', ''),
(17, 2, 2, 2, 'Mardi', '13:30:00', '16:30:00', 'Jeunes, Adultes, Seniors', ''),
(18, 2, 2, 2, 'Mercredi', '08:30:00', '11:30:00', 'Jeunes, Adultes, Seniors', ''),
(19, 2, 2, 2, 'Mercredi', '11:45:00', '14:45:00', 'Jeunes, Adultes, Seniors', ''),
(20, 2, 2, 2, 'Jeudi', '13:30:00', '17:30:00', 'Jeunes, Adultes, Seniors', ''),
(21, 2, 2, 2, 'Jeudi', '19:00:00', '22:00:00', 'Jeunes, Adultes, Seniors', ''),
(22, 3, 3, 2, 'Dimanche', '14:00:00', '17:00:00', 'Adultes', ''),
(23, 5, 4, 3, 'Mercredi', '10:00:00', '12:00:00', 'Familles', '1er mercredi de chaque période de vacances scolaires'),
(24, 27, 18, 3, 'Jeudi', '21:00:00', '23:00:00', 'Adultes, Seniors', ''),
(25, 27, 18, 3, 'Vendredi', '21:00:00', '23:00:00', 'Adultes, Seniors', ''),
(26, 6, 5, 4, 'Mercredi', '14:00:00', '16:00:00', 'Jeunes', 'Davantage d ateliers pendant les vacances scolaires'),
(27, 6, 5, 4, 'Mercredi', '16:00:00', '18:00:00', 'Jeunes', 'Davantage d ateliers pendant les vacances scolaires'),
(28, 15, 13, 4, 'Se renseigner auprès de l association', '09:00:00', '20:00:00', 'Tout public', ''),
(29, 6, 5, 5, 'Mercredi', '18:30:00', '21:30:00', 'Jeunes', 'Davantage d ateliers pendant les vacances scolaires'),
(30, 6, 5, 5, 'Vendredi', '18:30:00', '21:30:00', 'Jeunes', 'Davantage d ateliers pendant les vacances scolaires'),
(31, 7, 6, 6, 'Mardi', '14:00:00', '19:30:00', 'Tout public', ''),
(32, 7, 6, 6, 'Mercredi', '09:30:00', '18:00:00', 'Tout public', ''),
(33, 7, 6, 6, 'Jeudi', '14:00:00', '18:00:00', 'Tout public', ''),
(34, 7, 6, 6, 'Vendredi', '14:00:00', '19:30:00', 'Tout public', ''),
(35, 7, 6, 6, 'Samedi', '09:30:00', '17:00:00', 'Tout public', ''),
(36, 8, 7, 7, 'Se renseigner auprès du conservatoire', '09:00:00', '20:00:00', 'Enfants, Jeunes', 'Hors vacances scolaires'),
(37, 10, 7, 7, 'Se renseigner auprès du conservatoire', '09:00:00', '20:00:00', 'Enfants, Jeunes', 'Hors vacances scolaires'),
(38, 18, 7, 7, 'Se renseigner auprès du conservatoire', '09:00:00', '20:00:00', 'Enfants, Jeunes', 'Hors vacances scolaires'),
(39, 19, 7, 7, 'Se renseigner auprès du conservatoire', '09:00:00', '20:00:00', 'Enfants, Jeunes', 'Hors vacances scolaires'),
(40, 24, 7, 7, 'Se renseigner auprès du conservatoire', '09:00:00', '20:00:00', 'Enfants, Jeunes', 'Hors vacances scolaires'),
(41, 25, 7, 7, 'Se renseigner auprès du conservatoire', '09:00:00', '20:00:00', 'Enfants, Jeunes', 'Hors vacances scolaires'),
(42, 8, 8, 8, 'Se renseigner auprès de l association', '09:00:00', '20:00:00', 'Enfants, Jeunes, Adultes', ''),
(43, 20, 8, 8, 'Se renseigner auprès de l association', '09:00:00', '20:00:00', 'Enfants, Jeunes, Adultes', ''),
(44, 26, 8, 8, 'Se renseigner auprès de l association', '09:00:00', '20:00:00', 'Enfants, Jeunes, Adultes', ''),
(45, 9, 9, 8, 'Mardi', '20:15:00', '22:30:00', 'Adultes', ''),
(46, 14, 12, 8, 'Lundi', '09:00:00', '12:30:00', 'Seniors', ''),
(47, 14, 12, 8, 'Jeudi', '09:00:00', '12:30:00', 'Seniors', ''),
(48, 14, 12, 8, 'Vendredi', '09:00:00', '12:30:00', 'Seniors', ''),
(49, 13, 11, 9, 'Se renseigner auprès de l association', '09:00:00', '20:00:00', 'Enfants, Jeunes, Adultes', ''),
(50, 16, 14, 10, 'Lundi', '09:00:00', '12:30:00', 'Seniors', ''),
(51, 16, 14, 10, 'Mardi', '09:00:00', '12:30:00', 'Seniors', ''),
(52, 16, 14, 10, 'Jeudi', '14:00:00', '17:30:00', 'Seniors', ''),
(53, 16, 14, 10, 'Vendredi', '09:00:00', '12:30:00', 'Seniors', ''),
(54, 17, 15, 11, 'Mardi', '14:00:00', '18:00:00', 'Tout public', ''),
(55, 17, 15, 11, 'Mercredi', '09:00:00', '12:00:00', 'Tout public', ''),
(56, 17, 15, 11, 'Mercredi', '14:00:00', '18:00:00', 'Tout public', ''),
(57, 17, 15, 11, 'Jeudi', '09:00:00', '12:00:00', 'Tout public', ''),
(58, 17, 15, 11, 'Jeudi', '14:00:00', '18:00:00', 'Tout public', ''),
(59, 17, 15, 11, 'Vendredi', '09:00:00', '12:00:00', 'Tout public', ''),
(60, 17, 15, 11, 'Vendredi', '14:00:00', '17:00:00', 'Tout public', ''),
(61, 17, 15, 11, 'Samedi', '09:00:00', '12:00:00', 'Tout public', ''),
(62, 17, 15, 11, 'Samedi', '13:30:00', '16:30:00', 'Tout public', ''),
(63, 22, 12, 12, 'Mercredi', '10:00:00', '12:00:00', 'Seniors', ''),
(64, 23, 17, 13, 'Mardi', '20:30:00', '22:00:00', 'Adultes, Seniors', ''),
(65, 21, 16, 14, 'Mercredi', '16:00:00', '18:00:00', 'Enfants', ''),
(66, 21, 16, 14, 'Mercredi', '18:00:00', '22:00:00', 'Jeunes', '');

-- --------------------------------------------------------


--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `NumStatut` int(11) NOT NULL AUTO_INCREMENT,
  `NomStatut` varchar(50) NOT NULL,
  PRIMARY KEY (`NumStatut`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`NumStatut`, `NomStatut`) VALUES
(1, 'président'),
(2, 'présidente'),
(3, 'directeur'),
(4, 'directrice'),
(5, 'vice-président'),
(6, 'vice-présidente'),
(7, 'trésorier'),
(8, 'trésoriere'),
(9, 'secrétaire'),
(10, 'responsable'),
(11, 'vice-président1'),
(12, 'vice-présidente1'),
(13, 'vice-président2'),
(14, 'vice-présidente2'),
(15, 'animateur'),
(16, 'animatrice'),
(17, 'professeur'),
(18, 'professeure'),
(19, 'assistant'),
(20, 'assistante'),
(21, 'chef de chœur'),
(22, 'réfèrent local'),
(23, 'réfèrente locale'),
(24, 'non-renseigné');

-- --------------------------------------------------------

--
-- Structure de la table `typeactivité`
--

DROP TABLE IF EXISTS `typeactivité`;
CREATE TABLE IF NOT EXISTS `typeactivité` (
  `NumTypeActivité` int(11) NOT NULL AUTO_INCREMENT,
  `NomTypeActivité` varchar(50) NOT NULL,
  PRIMARY KEY (`NumTypeActivité`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `typeactivité`
--

INSERT INTO `typeactivité` (`NumTypeActivité`, `NomTypeActivité`) VALUES
(1, 'Accompagnement à la scolarité'),
(2, 'Arts graphiques et plastiques'),
(3, 'Atelier d’écriture'),
(4, 'Ateliers enfants'),
(5, 'Ateliers familles'),
(6, 'Ateliers jeunes'),
(7, 'Bibliotheques'),
(8, 'Chant et/ou Chorale'),
(9, 'Chorale'),
(10, 'Danse'),
(11, 'Environnement'),
(12, 'Informatique – Multimédia'),
(13, 'Loisirs rencontres, visites et excursions'),
(14, 'Loisirs séniors'),
(15, 'Ludothèques'),
(16, 'Musique'),
(17, 'Philosophie'),
(18, 'Photographie'),
(19, 'Théâtre'),
(20, 'Théâtre et chant');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `NumUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `PrenomUtilisateur` varchar(100) DEFAULT NULL,
  `NomUtilisateur` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Mdp` varchar(255) DEFAULT NULL,
  `Role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NumUtilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`NumUtilisateur`, `PrenomUtilisateur`, `NomUtilisateur`, `Email`, `Mdp`, `Role`) VALUES
(1, 'Timothée', 'Moulin', 'admin@mail.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', 'Admin'),
(2, 'Truc', 'Machin', 'truc@mail.com', '94f62b7f08511803ed0605d0bcbf622b23e4eba2', 'Editeur');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `activités`
--
ALTER TABLE `activités`
  ADD CONSTRAINT `activités_ibfk_1` FOREIGN KEY (`NumTypeActivité`) REFERENCES `typeactivité` (`NumTypeActivité`);

--
-- Contraintes pour la table `avoirrole`
--
ALTER TABLE `avoirrole`
  ADD CONSTRAINT `avoirrole_ibfk_1` FOREIGN KEY (`NumAssociation`) REFERENCES `associations` (`NumAssociation`),
  ADD CONSTRAINT `avoirrole_ibfk_2` FOREIGN KEY (`NumContact`) REFERENCES `contact` (`NumContact`),
  ADD CONSTRAINT `avoirrole_ibfk_3` FOREIGN KEY (`NumStatut`) REFERENCES `statut` (`NumStatut`);

--
-- Contraintes pour la table `pratique`
--
ALTER TABLE `pratique`
  ADD CONSTRAINT `pratique_ibfk_1` FOREIGN KEY (`JoursSemaine`) REFERENCES `joursemaine` (`JoursSemaine`),
  ADD CONSTRAINT `pratique_ibfk_2` FOREIGN KEY (`NumAssociation`) REFERENCES `associations` (`NumAssociation`),
  ADD CONSTRAINT `pratique_ibfk_3` FOREIGN KEY (`NumActivité`) REFERENCES `activités` (`NumActivité`),
  ADD CONSTRAINT `pratique_ibfk_4` FOREIGN KEY (`NumLieu`) REFERENCES `lieux` (`NumLieu`);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
