
<?php
class Contact{
private $AdresseElectroniqueContact;
private $CivilitéContact;
private $NomContact;
private $TelephoneContact;
private $numContact;


/**
 * Get the value of AdresseElectroniqueContact
 */ 
public function getAdresseElectroniqueContact()
{
return $this->AdresseElectroniqueContact;
}

/**
 * Set the value of AdresseElectroniqueContact
 *
 * @return  self
 */ 
public function setAdresseElectroniqueContact($AdresseElectroniqueContact)
{
$this->AdresseElectroniqueContact = $AdresseElectroniqueContact;

return $this;
}

/**
 * Get the value of CivilitéContact
 */ 
public function getCivilitéContact()
{
return $this->CivilitéContact;
}

/**
 * Set the value of CivilitéContact
 *
 * @return  self
 */ 
public function setCivilitéContact($CivilitéContact)
{
$this->CivilitéContact = $CivilitéContact;

return $this;
}

/**
 * Get the value of NomContact
 */ 
public function getNomContact()
{
return $this->NomContact;
}

/**
 * Set the value of NomContact
 *
 * @return  self
 */ 
public function setNomContact($NomContact)
{
$this->NomContact = $NomContact;

return $this;
}

/**
 * Get the value of TelephoneContact
 */ 
public function getTelephoneContact()
{
return $this->TelephoneContact;
}

/**
 * Set the value of TelephoneContact
 *
 * @return  self
 */ 
public function setTelephoneContact($TelephoneContact)
{
$this->TelephoneContact = $TelephoneContact;

return $this;
}


}





?>
