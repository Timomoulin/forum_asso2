<?php
class Activite {
    private $NumActivité;
    private $NomActivité;
    private $Associations;
    private $Pratiques;
    
    public function __construct()
    {

    }

    function getNumActivité()
    {
        return $this->NumActivité;
    }
    function getNomActivité()
    {
        return $this->NomActivité;
    }
    function getAssociations()
    {
        $lesPratiques=$this->getPratiques();
        $lesAssos=array();
        foreach($lesPratiques as $unePratique)
        {
            $asso=$unePratique->getAssociation();
            if(count($lesAssos)==0)
            {
                array_push($lesAssos,$unePratique->getAssociation());
            }
            else{
                $ajout=true;
                foreach($lesAssos as $uneAsso)
                {
                    if($uneAsso->getNumAssociation()==$asso->getNumAssociation())
                    {
                        $ajout=false;
                    }
                }
                if($ajout==true)
                {
                    array_push($lesAssos,$unePratique->getAssociation());
                }
            }
        }
        return $lesAssos;
    }

    function setNumActivité ($unId)
    {
        $this->NumActivité=$unId;
    }
    function setNomActivité ($unNom)
    {
        $this->NomActivité=$unNom;
    }
    function setAssociations ($desAsso)
    {
        $this->Associations=$desAsso;
    }

    /**
     * Get the value of Pratiques
     */ 
    public function getPratiques()
    {
        return $this->Pratiques;
    }

    /**
     * Set the value of Pratiques
     *
     * @return  self
     */ 
    public function setPratiques($Pratiques)
    {
        $this->Pratiques = $Pratiques;

        return $this;
    }
}
?>