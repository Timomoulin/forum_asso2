<?php
class Association {
    private $NumAssociation;
    private $NomAssociation; 
    private $AssociationPrivée;
    private $AdresseElectroniqueAssociation;
    private $SiteAssociation;
    private $Description;
    private $Pratiques;
    //TODO 

    /**
     * Constructeur de la classe Association
     */
    public function __construct ()
    {

    }

    public function getNumAssociation ()
    {
        return $this->NumAssociation;
    }
    public function getNomAssociation(){
        return $this->NomAssociation;
    }
    public function getAssociationPrivée(){
        return $this->AssociationPrivée;
    }
    public function getAdresseElectroniqueAssociation()
    {
        return $this->AdresseElectroniqueAssociation;
    }
    public function getSiteAssociation()
    {
        return $this->SiteAssociation;
    }
    public function getDescription()
    {
        return $this->Description;
    }

    public function setNumAssociation($uneId)
    {
        $this->NumAssociation=$uneId;
    }

    public function setNomAssociation($unNom)
    {
        $this->NomAssociation=$unNom;
    }
    public function setAssociationPrivée($estPrivée)
    {
        $this->AssociationPrivée=$estPrivée;
    }
    public function setAdresseElectroniqueAssociation($unEmail)
    {
        $this->AdresseElectroniqueAssociation=$unEmail;
    }
    public function setSiteAssociation($unSite)
    {
        $this->SiteAssociation=$unSite;
    }
    public function setDescription($uneDescription)
    {
        $this->Description=$uneDescription;
    }


    /**
     * Get the value of Pratiques
     */ 
    public function getPratiques()
    {
        return $this->Pratiques;
    }

    /**
     * Set the value of Pratiques
     *
     * @return  self
     */ 
    public function setPratiques($Pratiques)
    {
        $this->Pratiques = $Pratiques;

        return $this;
    }
}
?>