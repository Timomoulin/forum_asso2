<?php
class Pratique{
    private $NumActivité; // index Activite
    private $NumAssociation; // index Asso
    private $NumPratique;
    private $NumLieu; // index Lieu
    private $JoursSemaine;
    private $HeureDébut;
    private $HeureFin;
    private $TypePublic;
    private $Detail;
    private $Activite; //objet Activite
    private $Association; //objet Association
    // private $Lieu; // objet Lieu

    /**
     * Get the value of NumAssociation
     */ 
    public function getNumAssociation()
    {
        return $this->NumAssociation;
    }

    /**
     * Set the value of NumAssociation
     *
     * @return  self
     */ 
    public function setNumAssociation($NumAssociation)
    {
        $this->NumAssociation = $NumAssociation;

        return $this;
    }

    /**
     * Get the value of NumActivité
     */ 
    public function getNumActivité()
    {
        return $this->NumActivité;
    }

    /**
     * Set the value of NumActivité
     *
     * @return  self
     */ 
    public function setNumActivité($NumActivité)
    {
        $this->NumActivité = $NumActivité;

        return $this;
    }

    /**
     * Get the value of NumPratique
     */ 
    public function getNumPratique()
    {
        return $this->NumPratique;
    }

    /**
     * Set the value of NumPratique
     *
     * @return  self
     */ 
    public function setNumPratique($NumPratique)
    {
        $this->NumPratique = $NumPratique;

        return $this;
    }

    /**
     * Get the value of NumLieu
     */ 
    public function getNumLieu()
    {
        return $this->NumLieu;
    }

    /**
     * Set the value of NumLieu
     *
     * @return  self
     */ 
    public function setNumLieu($NumLieu)
    {
        $this->NumLieu = $NumLieu;

        return $this;
    }

    /**
     * Get the value of JoursSemaine
     */ 
    public function getJoursSemaine()
    {
        return $this->JoursSemaine;
    }

    /**
     * Set the value of JoursSemaine
     *
     * @return  self
     */ 
    public function setJoursSemaine($JoursSemaine)
    {
        $this->JoursSemaine = $JoursSemaine;

        return $this;
    }

    /**
     * Get the value of HeureDébut
     */ 
    public function getHeureDébut()
    {
        return $this->HeureDébut;
    }

    /**
     * Set the value of HeureDébut
     *
     * @return  self
     */ 
    public function setHeureDébut($HeureDébut)
    {
        $this->HeureDébut = $HeureDébut;

        return $this;
    }

    /**
     * Get the value of HeureFin
     */ 
    public function getHeureFin()
    {
        return $this->HeureFin;
    }

    /**
     * Set the value of HeureFin
     *
     * @return  self
     */ 
    public function setHeureFin($HeureFin)
    {
        $this->HeureFin = $HeureFin;

        return $this;
    }

    /**
     * Get the value of TypePublic
     */ 
    public function getTypePublic()
    {
        return $this->TypePublic;
    }

    /**
     * Set the value of TypePublic
     *
     * @return  self
     */ 
    public function setTypePublic($TypePublic)
    {
        $this->TypePublic = $TypePublic;

        return $this;
    }

    /**
     * Get the value of Detail
     */ 
    public function getDetail()
    {
        return $this->Detail;
    }

    /**
     * Set the value of Detail
     *
     * @return  self
     */ 
    public function setDetail($Detail)
    {
        $this->Detail = $Detail;

        return $this;
    }

    /**
     * Get the value of Activite
     */ 
    public function getActivite()
    {
        return $this->Activite;
    }

    /**
     * Set the value of Activite
     *
     * @return  self
     */ 
    public function setActivite($Activite)
    {
        $this->Activite = $Activite;

        return $this;
    }

    /**
     * Get the value of Association
     */ 
    public function getAssociation()
    {
        return $this->Association;
    }

    /**
     * Set the value of Association
     *
     * @return  self
     */ 
    public function setAssociation($Association)
    {
        $this->Association = $Association;

        return $this;
    }

    /**
     * Get the value of Lieu
     */ 
    public function getLieu()
    {
        return $this->Lieu;
    }

    /**
     * Set the value of Lieu
     *
     * @return  self
     */ 
    public function setLieu($Lieu)
    {
        $this->Lieu = $Lieu;

        return $this;
    }
}
?>