<?php


class PratiqueManager {
    private $assoManager;

    private $connex;
    function __construct($db)
    {
       $this->connex=$db;
        $this->assoManager = new AssociationManager($db);
    }

    function getPratiqueBy($nomCol, $valeurCol)
    {
        try {
            $sql = $this->connex->prepare(" Select distinct * from pratique natural join associations natural join activités natural join lieux  where $nomCol=:valeurCol");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Pratique');
            $sql->bindParam(':valeurCol', $valeurCol);
            $sql->execute();
            $resultat = ($sql->fetchAll());
            foreach($resultat as $unePratique)
            {
                $asso=$this->assoManager->getAssociationBy("NumAssociation",$unePratique->getNumAssociation());
               
                $unePratique->setAssociation($asso[0]);
              
            }
           
            return $resultat;
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    function deletePratique($id)
    {
        try {
        $sql2 = $this->connex->prepare("DELETE FROM pratique where NumAssociation=:uneId");
        $sql2->bindParam(":uneId", $id);
        $sql2->execute();
        }
     catch (PDOException $error) {
        echo $error->getMessage();
    }
    }

}

?>