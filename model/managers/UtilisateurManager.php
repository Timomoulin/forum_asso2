<?php
class UtilisateurManager 
{
    private $connex;
    function __construct($db)
    {
        $this->connex=$db;
    }

    function verificationLogin($email,$mdp)
    {
        try {
            $sql = $this->connex->prepare("SELECT * FROM utilisateur where Email=:email and Mdp=:mdp");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Utilisateur'); // la class correspond à la classe de la BDD
            $sql->bindParam(":email", $email); // bindParam est pour eviter l'injection de code SQL
            $sql->bindParam(":mdp", $mdp);
            $sql->execute();
            $resultat = ($sql->fetchAll());
            var_dump($email,$mdp);
            if(count($resultat)>=1)
            {
                return $resultat[0];
            }
            else{
                return false;
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}

?>
