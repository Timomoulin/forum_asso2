<?php
require_once("ActiviteManager.php");
class TypeManager 
{
    private $activiteManager;
    private $connex;
    function __construct($db)
    {
        $this->connex=$db;
        $this->activiteManager = new ActiviteManager($db);
    }

    function getAllTypes()
    {
        try {
            $sql = $this->connex->prepare("SELECT * FROM typeactivité");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Type');
            $sql->execute();

            $lesTypes = array();
            $resultat = ($sql->fetchAll());

            foreach ($resultat as $unType) {

                $lesActivites = $this->activiteManager->getActivitesBy("NumTypeActivité", $unType->getNumTypeActivité());
                $unType->setActivités($lesActivites);
                array_push($lesTypes, $unType); // Ajouter un type a la liste de types
            }
            return $lesTypes;
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function getTypesBy($nomCol, $valeurCol)
    {
        try {
            $sql = $this->connex->prepare("SELECT * FROM typeactivité WHERE $nomCol = :valeurCol");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Type');
            $sql->bindParam(":valeurCol", $valeurCol);
            $sql->execute();


            $resultat = ($sql->fetchAll());

            foreach ($resultat as $unType) {
                $lesActivites = $this->activiteManager->getActivitesBy("NumTypeActivité", $unType->getNumTypeActivité());
                $unType->setActivités($lesActivites);
            }
            return $resultat;
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function addTypes($nomType)
    {
        try {
            $sql = $this->connex->prepare("INSERT INTO typeActivité values(null,:nomType)");
            $sql->bindParam(":nomType", $nomType);
            $sql->execute();
            //Verification de l'ajout
            $newId = $this->connex->lastInsertId(); // Permet de recuperer l'id du dernier ajout
            $verifType = $this->getTypesBy("NumTypeActivité", $newId)[0]; // Extraction du dernier ajout
            if ($verifType->getNomTypeActivité() == $nomType) // Verification que le nom du dernier ajout==$nomType
            {
                return true; //Ajout OK
            } else {
                return false; //Ajout a échoué
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function updateTypes($idType, $nomType)
    { 
            try {
                $sql = $this->connex->prepare("UPDATE typeActivité SET NomTypeActivité = :nomType where NumTypeActivité=:uneId");
                $sql->bindParam(":nomType", $nomType);
                $sql->bindParam(":uneId", $idType);
                $sql->execute();
                //Verification
                $verifType = $this->getTypesBy("NumTypeActivité", $idType)[0]; // Extraction du Type
                if ($verifType->getNomTypeActivité() == $nomType) // Verification que le nom du dernier ajout==$nomType
                {
                    return true; //Modification OK
                } else {
                    return false; //Modification a échoué
                }
            } catch (PDOException $error) {
                echo $error->getMessage();
            }
        
    }
    function deleteTypes($idType)
    {
        try {
            $leType = $this->getTypesBy("NumTypeActivité", $idType)[0];
            foreach ($leType->getActivités() as $activité) {
                var_dump($this->activiteManager->deleteActivites($activité->getNumActivité()));
            }
            $sql2 = $this->connex->prepare("DELETE FROM typeActivité where NumTypeActivité=:uneId");
            $sql2->bindParam(":uneId", $idType);
            var_dump($sql2);
            $sql2->execute();
            //Verification
            $verifType = $this->getTypesBy("NumTypeActivité", $idType); // Extraction du Type
            if (count($verifType) == 0) // Verification 
            {
                return true; //Modification OK
            } else {
                return false; //Modification a échoué
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
