<?php
class AssociationManager 
{
    private $connex;
    function __construct($db)
    {
       $this->connex=$db;
        
    }

    function getAssociationBy($nomCol, $valeurCol)
    {
        try {
            $sql = $this->connex->prepare(" Select distinct * from associations natural join pratique  where $nomCol=:valeurCol");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Association');
            $sql->bindParam(':valeurCol', $valeurCol);
            $sql->execute();
            $resultat = ($sql->fetchAll());
            // $lesPratiques=array();
            // foreach($resultat as $uneAssociation)
            // {
            //     $unePratique=$this->pratiqueManager->getPratiqueBy("NumAssociation",$uneAssociation->getNumAssociation());
            //     array_push($lesPratiques,$unePratique);
            // }
            
            return $resultat;
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    function addAssociations($nom, $prive, $email, $site, $description)
    {
        try {
            $sql = $this->connex->prepare("INSERT INTO associations VALUES(null,:nom,:prive2,:email,:site,:description)");
            $sql->bindParam(":nom", $nom);
            $sql->bindParam(":prive2", $prive);
            
            $sql->bindParam(":email", $email);
            $sql->bindParam(":site", $site);
            $sql->bindParam(":description", $description);
            $sql->execute();
            //Verification de la modif
            return true;
        } catch (PDOException $error) {
            echo $error->getMessage();
            return false;
        }
    }

    function updateAssociations($num, $nom, $prive, $email, $site, $description)
    { 
            try {
                $sql = $this->connex->prepare("UPDATE associations SET NomAssociation = :nom,AssociationPrivée=:prive2, AdresseElectroniqueAssociation=:email, SiteAssociation=:site,Description=:description where NumAssociation=:num");
                $sql->bindParam(":nom", $nom);
                $sql->bindParam(":num", $num);
                $sql->bindParam(":prive2", $prive);
                
                $sql->bindParam(":email", $email);
                $sql->bindParam(":site", $site);
                $sql->bindParam(":description", $description);
                $sql->execute();
                //Verification de la modif
                $verif = $this->getAssociationBy("NumAssociation", $num)[0]; // Extraction de l'activite
                if ($verif->getNomAssociation() == $nom) // Verification que le nom du dernier ajout==$nom
                {
                    return true; //Modification OK
                } else {
                    return false; //Modification a échoué
                }
            } catch (PDOException $error) {
                echo $error->getMessage();
            }
        
    }
    function deleteAssociation($id)
    {
        try {
            $sql1 = $this->connex->prepare("DELETE FROM proposer where NumAssociation=:uneId");
            $sql1->bindParam(":uneId", $id);
            $sql1->execute();

            

            $sql3 = $this->connex->prepare("DELETE FROM activités where NumAssociation=:uneId");
            $sql3->bindParam(":uneId", $id);
            $sql3->execute();

            //Verification de l'ajout
            $verif = $this->getAssociationBy("NumAssociation", $id); // Extraction du Type
            if (count($verif) == 0) // Verification 
            {
                return true; //Modification OK
            } else {
                return false; //Modification a échoué
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
