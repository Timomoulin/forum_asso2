<?php
require("classes/Type.class.php");
require("classes/Activite.class.php");
require("classes/Association.class.php");
require("classes/Pratique.class.php");
require("classes/Utilisateur.class.php");
require("classes/Contact.class.php");
require("managers/AssociationManager.php");
require("managers/ActiviteManager.php");
require("managers/TypeManager.php");
require("managers/PratiqueManager.php");
require("managers/UtilisateurManager.php");
require("managers/ContactManager.php");
class BDD
{
    public $connex;

    function __construct()
    {
        $this->connex = new PDO("mysql:host=localhost;dbname=forum_asso4", "root", "", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $this->connex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
    }
}

