<?php $title = 'Login'; ?>

<?php ob_start(); ?>

<div class="container d-flex flex-column col-md-8 col-lg-6 p-3">
<h1 class="align-self-center">Login</h1>
    <?php
    if(isset($_GET["resultat"]))
    {
        $resultat=test_input($_GET["resultat"]);
        if($resultat==0)
        {
            echo ('<h2 class="text-danger"> Echec </h2>');
        }
    }
    ?>
<form novalidate action="./?action=verifLogin&amp;" method="post">
<input type="hidden" name="token" value="<?=$_SESSION['token']?>"/>
<label for="inputEmail">Email :</label>
<input type="email" id="inputEmail" name="email" minlength="6"  required class="form-control my-2">
<label for="">MDP :</label>
<input type="password" id="inputMDP" name="mdp" minlength="6" required class="form-control my-2">
<!-- type="password est pour queon masque les symboles du mot de passe -->
<button class="btn btn-primary my-2 ">Envoyer</button>
</form>
<br>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
