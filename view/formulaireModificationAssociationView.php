<?php $title = "Formulaire de modification d'une association"; ?>

<?php ob_start(); ?>
<div class="container main d-flex flex-column align-items-center">
    <h1>Formulaire de modification d'une association</h1>
    <hr>
    <form action="./?path=gestionnaire&action=updateAssociations&amp;" method="post" class="d-flex flex-column justify-content-around align-items-stretch col-lg-6">
        <input type="number" name="idAssociation" required hidden value="<?php echo ($association->getNumAssociation()); ?>">
        <label for="typeNom">Nom de l'assocation :</label>
        <?php echo ('<input type="text" name="nomAsso" class="form-control m-2 " minlength="3" placeholder="Saisir le nom" value="' . $association->getNomAssociation() . '"">') ?>
        <label for="typeNom">Email l'assocation :</label>
        <?php echo ('<input type="email" name="email" class="form-control m-2 " minlength="3" placeholder="Saisir le mail" value="' . $association->getAdresseElectroniqueAssociation() . '"">') ?>
        <label for="typeNom">Site de l'assocation :</label>
        <?php echo ('<input type="url" name="site" class="form-control m-2 " minlength="3" placeholder="Saisir le nom" value="' . $association->getSiteAssociation() . '"">') ?>
        <label for="typeNom">Association privée ?</label>
        <?php
        if ($association->getAssociationPrivée() == true) {
            echo ('<input type="checkbox" checked name="prive" class="  m-2 " >');
        } else {
            echo ('<input type="checkbox" name="prive" class=" m-2 "  >');
        }
        ?>
        <?php  ?>
        <label for="typeNom">Description l'assocation :</label>
        <?php echo ('<textarea type="text" name="description" class="form-control m-2 " minlength="3" placeholder="Saisir le nom" value="">' . $association->getDescription() . '</textarea>') ?>


        <!-- TODO CSRF -->
        <button class="btn btn-primary m-2">Envoyer</button>
    </form>
    <hr>
    <br>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>