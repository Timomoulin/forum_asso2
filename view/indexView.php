<?php $title = 'Home'; ?>

<?php ob_start(); ?>
<div class="container">
<h1>Template MVC</h1>

<p>indexView.php (ou une autre vue) donne le content et title a template.php</p>
<p>Dans la racine le fichier index.php est le routeur il déclenche une fonction du controlleur (controlleur/controlleur.php)</p>
<p>Le controlleur doit normalement allez chercher les données du model issue de la bdd et renvoyer vers une vue (View exemple : view/indexView.php ou page2View.php</p>
<p>La vue ne fait que créer 2 variables titre et content le titre de la page et le contenue est les inseres dans template.php</p>
<a href=".?action=Associations">Voir les associations</a>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>