<?php $title = "Formulaire de modification d'une association"; ?>

<?php ob_start(); ?>
<div class="container main d-flex flex-column align-items-center">
    <h1>Formulaire de modification d'une association</h1>
    <hr>
    <form action="./?path=gestionnaire&action=addAsso&amp;" method="post" class="d-flex flex-column justify-content-around align-items-stretch col-lg-6">
        <label for="typeNom">Nom de l'assocation :</label>
        <input type="text" name="nomAsso" class="form-control m-2 " minlength="3" placeholder="Saisir le nom" value="">
        <label for="typeNom">Email l'assocation :</label>
        <input type="email" name="email" class="form-control m-2 " minlength="3" placeholder="Saisir le mail" value="">
        <label for="typeNom">Site de l'assocation :</label>
       <input type="url" name="site" class="form-control m-2 " minlength="3" placeholder="Saisir le nom" value="">
        <label for="typeNom">Association privée ?</label>
        <input type="checkbox"  name="prive" class="  m-2 " >
        <label for="typeNom">Description l'assocation :</label>
        <textarea type="text" name="description" class="form-control m-2 " minlength="3" placeholder="Saisir le nom" value=""></textarea>


        <!-- TODO CSRF -->
        <button class="btn btn-primary m-2">Envoyer</button>
    </form>
    <hr>
    <br>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>