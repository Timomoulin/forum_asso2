<?php $title =$uneAsso->getNomAssociation() ; ?>

<?php ob_start(); ?>
<div class="container">
<?php 
echo ("<h1>".$uneAsso->getNomAssociation()."</h1>"); 
echo("Description = ".$uneAsso->getDescription()."</br>");
if($uneAsso->getAdresseElectroniqueAssociation())
{
    echo("Email = ".$uneAsso->getAdresseElectroniqueAssociation()."</br>");
}
if($uneAsso->getSiteAssociation())
{
    echo("Site =".$uneAsso->getSiteAssociation()."</br>");
}

// var_dump($lesPratiques);
if(isset($role))
{
if($role=="Admin"||$role=="Gestionnaire")
{
echo("<button class='btn btn-danger'>Ajouter une pratique</button>");
}
}
echo("<table class='border border-3 border-dark'>");
echo("<thead><tr class='border border-2 border-dark'>");
echo("<td class='border border-1 border-dark'>Type Public </td>");
echo("<td class='border border-1 border-dark'>Jours</td>");
echo("<td class='border border-1 border-dark'>Heure Début</td>");
echo("<td class='border border-1 border-dark'>Heure Fin</td>");
echo("<td class='border border-1 border-dark'>Nom du lieu</td>");
echo("<td class='border border-1 border-dark'>Adresse</td>");
echo("</tr></thead>");
foreach($lesPratiques as $unePratique)
{
    echo("<tr>");
    echo("<td class='p-1 border border-1 border-dark'> ".$unePratique->getTypePublic()." </td>");
    echo("<td class='p-1 border border-1 border-dark'> ".$unePratique->getJoursSemaine()." </td>");
    echo("<td class='p-1 border border-1 border-dark'>".$unePratique->getHeureDébut()."</td>");
    echo("<td class='p-1 border border-1 border-dark'>".$unePratique->getHeureFin()."</td>");
    echo("<td class='p-1 border border-1 border-dark'>".$unePratique->NomLieu."</td>");
    echo("<td class='p-1 border border-1 border-dark'>".$unePratique->AdresseLieu."</td>");
    if(isset($role))
    {
    if($role=="Admin"||$role=="Gestionnaire")
    {
    echo("<td><button class='btn btn-danger'>Modifier</button></td>");
    echo("<td><button class='btn btn-danger'>Supprimer</button></td>");
    }
}
    echo("</tr>");
}
echo("</table>");

foreach($lesContacts as $unContact)
{
    echo("<div>".$unContact->NomStatut." ".$unContact->getCivilitéContact()." ".$unContact->getNomContact()." ".$unContact->getTelephoneContact()." ".$unContact->getAdresseElectroniqueContact()."</div>");
}

?>

</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>