<?php $title = 'Associations'; ?>

<?php ob_start(); ?>
<div class="container col-lg-8 p-2">
    <h1>Les associations</h1>
   <?php if(isset($role)&&($role=="Admin"||$role=="Gestionnaire"))
            {
    echo '<a href="./?path=gestionnaire&action=FormulaireAjoutType" class="btn btn-success m-1">Ajouter un type</a> <br />';
            }
    ?>
    <div class="accordion" id="accordionType">
        <?php
        foreach ($lesTypes as $unType) {
            $lienTypemod = "./?path=gestionnaire&action=FormulaireModifType&id=" . $unType->getNumTypeActivité() . "";
            $lienTypedel = "./?path=gestionnaire&action=deleteTypes&id=" . $unType->getNumTypeActivité() . "";
            $lienActivitesAjout = "./?path=gestionnaire&action=FormulaireAjoutActivites&idType=" . $unType->getNumTypeActivité() . "";
            echo ("<div class='my-3 p-2 accordion-item'>");
            echo (' <a class="accordion-button collapsed text-light bg-color2" type="button" data-bs-toggle="collapse" data-bs-target="#collapse' . $unType->getNumTypeActivité() . '" aria-expanded="true" aria-controls="collapse' . $unType->getNumTypeActivité() . '">');
            echo ("<h3 class='d-flex justify-content-center p-1 text-uppercase'>");
            echo ($unType->getNomTypeActivité());
            echo ("</h3>");
            echo ('</a>');
            if(isset($role)&&($role=="Admin"||$role=="Gestionnaire"))
            {
                echo ("<a href=$lienTypemod class='btn btn-success m-1'>Modifier</a> ");
                echo ("<a href=$lienTypedel class='btn btn-success m-1 supp'>Effacer</a>");
                echo ("<a href=$lienActivitesAjout class='btn btn-success m-1'>Ajouter une Activité</a> <br/>");
            }
           
            echo (' <div id="collapse' . $unType->getNumTypeActivité() . '" class="accordion-collapse collapse" data-bs-parent="#accordionType">
            <div class="accordion-body">');
            echo ("<ul class='m-2'> <div class='activite mx-3 border border-dark border-2'>");
            foreach ($unType->getActivités() as $uneActivite) {
                $lienActivitéMod = "./?path=gestionnaire&action=FormulaireModifActivites&id=" . $uneActivite->getNumActivité() . "";
                $lienActivitéDel = "./?path=gestionnaire&action=deleteActivites&id=" . $uneActivite->getNumActivité() . "";
                echo (" <h4 class='d-flex justify-content-start p-2 text-light bg-color1 text-uppercase'>" . $uneActivite->getNomActivité() . "</h4>");
                echo ("<ul>");
                if(isset($role)&&($role=="Admin"||$role=="Gestionnaire"))
                {
                echo ("<a href=$lienActivitéMod class='btn btn-success m-1'>Modifier</a>");
                echo ("<a href=$lienActivitéDel class='btn btn-success m-1 supp'>Effacer</a>");
                echo ("<a href='./?path=gestionnaire&action=FormulaireAjoutAsso' class='btn btn-success m-1'>Ajouter Association</a>");
                }
                echo ("<hr/>");
                foreach ($uneActivite->getAssociations() as $uneAsso) {
                    echo("<a href='./?action=Association&numAssociation=".$uneAsso->getNumAssociation()."'>");
                    echo ("<li>" . $uneAsso->getNomAssociation() . "</li>");
                    echo("</a>");
                    $lienAssoMod = "./?path=gestionnaire&action=FormulaireModificationAssociations&id=" . $uneAsso->getNumAssociation();
                    if(isset($role)&&($role=="Admin"||$role=="Gestionnaire"))
                    {
                    echo ("<a href='$lienAssoMod' class='btn btn-success m-1'>Modifier</a>");
                    echo ("<a href='#' class='btn btn-danger m-1 supp'>Effacer</a>");
                    echo ("<hr/>");
                    }
                }
                echo ("</ul>");
            }
            echo ("</div></div></div></ul>");
            echo ("</div>");
        }
        echo ("</div>")
        ?>
    </div>
    <?php $content = ob_get_clean(); ?>

    <?php require('template.php'); ?>