<?php


function mainController()
{
    if(isset( $_SESSION['token'] ))
    {
        $token= $_SESSION['token'] ;
    }

    if(isset( $_SESSION['role'] ))
    {
        $role= $_SESSION['role'] ;
    }
    require('model/bdd.php');

    if (isset($_GET['action'])) {
        $action = test_input($_GET['action']);
        $unPDO=new BDD(); // Création d'un objet PDO
        $co=$unPDO->connex; // Récuperation de la co a la BDD
        if ($action == 'Associations') {
            
            $uneBDD = new TypeManager($co);
            $lesTypes = $uneBDD->getAllTypes();
            require("view/associationsView.php");
        } else if ($action == "resultat" && isset($_GET['operation']) && isset($_GET['valeur'])) {
            $operation = test_input($_GET['operation']);
            $valeur = test_input($_GET['valeur']);
            require("view/resultatView.php");
        } 
        elseif($action=="Association"&&isset($_GET['numAssociation']))
        {
            $idAsso=test_input($_GET['numAssociation']);
            $assoManager=new AssociationManager($co);
            $pratiqueManager=new PratiqueManager($co);
            $contactManager=new ContactManager($co);
            //TODO CONTACT
            $uneAsso=$assoManager->getAssociationBy("NumAssociation",$idAsso)[0];
            $lesPratiques=$pratiqueManager->getPratiqueBy("NumAssociation",$idAsso);
            $lesContacts=$contactManager->getContactBy("NumAssociation",$idAsso);
            require("view/uneAssociationView.php");
        }
        elseif($action=="formlogin")
        {
            if(isset($_SESSION['id']))
            {
                $_SESSION = array();
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                    );
                    header("location:./");
                }
            }
            require("./view/loginView.php");
        }
        elseif($action=="verifLogin"){
            if ($_POST['token'] == $_SESSION['token']) { 
                $userManager= new UtilisateurManager($co);
                echo("token ok");
                $email=test_input($_POST['email']);
                $mdp=test_input($_POST['mdp']);
                $mdp=sha1($mdp);
                $resultat =$userManager->verificationLogin($email,$mdp);
                if( $resultat ==false)
                {
                    header("Location:./?action=formlogin&resultat=0");
                    die;
                }
                else {
                    $_SESSION['role']=$resultat->getRole();
                    $_SESSION['email']=$resultat->getEmail();
                    $_SESSION['id']=$resultat->getNumUtilisateur();
                    header("Location:./?action=resultat&operation=de%20Connexion&valeur=1");
                }
            }
        }

        else {
            require("./view/404.php");
        }
        $co=null;
    }
    else{
        require("./view/indexView.php");
    }
  
}
