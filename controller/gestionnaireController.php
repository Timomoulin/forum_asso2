<?php
function gestionnaireController()
{

    if (isset($_SESSION['role'])) {
        require_once('model/bdd.php');

        if (isset($_GET['action'])&&($_SESSION['role'] == "Admin" || $_SESSION['role'] == "Gestionnaire")) {
            $action = test_input($_GET['action']);
            $db=new BDD();
            $co=$db->connex;
            if ($action == "FormulaireAjoutType") {
                require("view/formulaireAjoutTypeView.php");
            } else if ($action == "FormulaireModifType" && isset($_GET['id'])) {
                $id = test_input($_GET['id']);
                $uneBDD = new TypeManager($co);
                $unType = $uneBDD->getTypesBy("NumTypeActivité", $id)[0];
                require("view/formulaireModificationTypeView.php");
            } else if ($action == 'addTypes' && isset($_POST['nomType'])) {

                $nom = test_input($_POST['nomType']);
                $uneBDD = new TypeManager($co);
                $resultat = $uneBDD->addTypes($nom);
                header("Location:./?action=resultat&operation=Ajout%20du%20type&valeur=$resultat");
            } else if ($action == 'updateTypes' && isset($_POST['nomType']) && isset($_POST['idType'])) {
                $nom = test_input($_POST['nomType']);
                $id = test_input($_POST['idType']);
                $uneBDD = new TypeManager($co);
                $resultat = $uneBDD->updateTypes($id, $nom);
                header("Location:./?action=resultat&operation=Modification%20du%20type&valeur=$resultat");
            } else if ($action == "deleteTypes" && isset($_GET['id'])) {
                $id = test_input($_GET['id']);
                $uneBDD = new TypeManager($co);
                $resultat = $uneBDD->deleteTypes($id);
                header("Location:./?action=resultat&operation=Suppresion%20du%20type&valeur=$resultat");
            } else if ($action == "FormulaireAjoutActivites" && isset($_GET['idType'])) {
                $idType = test_input($_GET['idType']);
                require("view/formulaireAjoutActiviteView.php");
            } else if ($action == "FormulaireModifActivites" && isset($_GET['id'])) {
                $idActivite = test_input($_GET['id']);
                $uneBDD = new ActiviteManager($co);
                $uneActivite = $uneBDD->getActivitesBy("NumActivité", $idActivite)[0];
                require("view/formulaireModificationActiviteView.php");
            } else if ($action == "addActivites" && isset($_POST['idType']) && isset($_POST['nomActivite'])) {
                $idType = test_input($_POST['idType']);
                $nomActivite = test_input($_POST['nomActivite']);
                $uneBDD = new ActiviteManager($co);
                $resultat = $uneBDD->addActivites($nomActivite, $idType);
                header("Location:./?action=resultat&operation=Ajout%20d'une%20activite&valeur=$resultat");
            } else if ($action == "updateActivites" && isset($_POST['idActivite']) && isset($_POST['nomActivite'])) {
                $idActivite = test_input($_POST['idActivite']);
                $nom = test_input($_POST['nomActivite']);
                $uneBDD = new ActiviteManager($co);
                $resultat = $uneBDD->updateActivites($idActivite, $nom);
                header("Location:./?action=resultat&operation=Modification%20Activite&valeur=$resultat");
            } else if ($action == "deleteActivites" && isset($_GET['id'])) {
                $id = test_input($_GET['id']);
                $uneBDD = new ActiviteManager($co);
             
                $resultat = $uneBDD->deleteActivites($id);
                header("Location:./?action=resultat&operation=Suppresion%20activite&valeur=$resultat");
            } elseif ($action == "FormulaireModificationAssociations" && isset($_GET['id'])) {
                $id = test_input($_GET['id']);
                $uneBDD = new AssociationManager($co);
                $association = $uneBDD->getAssociationBy("NumAssociation", $id)[0];
                require("view/formulaireModificationAssociationView.php");
            }
            elseif($action=="FormulaireAjoutAsso")
            {
                require("view/formulaireAjoutAsso.php");
            }
            elseif($action="addAsso")
            {
                $nom = test_input($_POST['nomAsso']);
                $email = test_input($_POST['email']);
                $site = test_input($_POST['site']);
                if (isset($_POST['prive'])) {
                    $prive = (bool) test_input($_POST['prive']);
                } else {
                    $prive = 0;
                }
                $description = test_input($_POST['description']);
                $uneBDD = new AssociationManager($co);

                $resultat=$uneBDD->addAssociations($nom,$prive,$email,$site,$description);
                header("Location:./?action=resultat&operation=Ajout%20association&valeur=$resultat");
            }
            elseif ($action == "updateAssociations") {
                $id = test_input($_POST['idAssociation']);
                $nom = test_input($_POST['nomAsso']);
                $email = test_input($_POST['email']);
                $site = test_input($_POST['site']);
                if (isset($_POST['prive'])) {
                    $prive = (bool) test_input($_POST['prive']);
                } else {
                    $prive = 0;
                }
                $description = test_input($_POST['description']);
                $uneBDD = new AssociationManager($co);
                $resultat = $uneBDD->updateAssociations($id, $nom, $prive, $email, $site, $description);
                header("Location:./?action=resultat&operation=Modification%20association&valeur=$resultat");
            } else {
                require("./view/404.php");
            }
            $co=null;
        }
        else {
            require("./view/404.php");
        }
    }
    else {
        require("./view/404.php");
    }
}
